def countchars(input):
    output_dict = {}
    for ch in input:
        if ch not in output_dict:
            output_dict[ch] = 1
        else:
            output_dict[ch] += 1
    return output_dict
