from bitarray import bitarray

def encode(input, weights):
    output = bitarray()
    if hasattr(input, '__iter__'):
        for i in input:
            output += weights[i]
    else:
        output = weights[input]
    return output

def decode(input, weights):
    output = ''
    return output

def encode_file(filename, weights):
    output = bitarray()
    for b in bytes_from_file(filename):
        output.append(encode(b, weights))
    return output

def bytes_from_file(filename, chunksize=8192):
    with open(filename, "rb") as f:
        while True:
            chunk = f.read(chunksize)
            if chunk:
                for b in chunk:
                    yield b
            else:
                break
