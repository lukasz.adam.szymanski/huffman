from bitarray import bitarray

class Wezel(object):
    def __init__(self, right, left):
        self.parent = None
        left.parent = self
        right.parent = self

        self.left = left
        self.right = right

        self.weight = left.weight + right.weight

    def __lt__(self, other):
        return self.weight < other.weight


class Lisc(Wezel):
    def __init__(self, symbol, weight):
        self.parent = None
        self.symbol = symbol
        self.weight = weight

    @property
    def code(self):
        code = ""
        n = self
        while n.parent is not None:
            codebit = "0" if n is n.parent.left else "1"
            code = codebit + code
            n = n.parent
        return code


class Drzewo(object):
    def __init__(self, weights):
        weights = [(key, value) for key, value in weights.items()]
        weights.sort(key=lambda x: x[1], reverse=True)
        leaves = [Lisc(*sw) for sw in weights]
        stos = leaves.copy()

        while len(stos) >= 2:
            stos.insert(0, Wezel(stos.pop(), stos.pop()))

        self.root = stos.pop()
        self.codebook = {l.symbol: bitarray(l.code) if len(leaves) > 1 else bitarray('0') for l in leaves}

