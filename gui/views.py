from django.shortcuts import render
from huffman.binarytree import Drzewo
from huffman.countchars import countchars
from huffman.huffman import encode

def podziel(file, krok):
    i = 0
    j = krok
    output = []
    while True:
        chunk = file[i:j]
        i += krok
        j += krok
        if chunk:
            output.append(chunk)
        else:
            break
    return output

# troche na kolanie
def zakoduj_plik(file, ilosc_bajtow):
    chunks = podziel(file, ilosc_bajtow)
    wagi = countchars(chunks)
    d = Drzewo(wagi)
    zakodowany = encode(chunks, d.codebook)
    bajty_przed = len(file)
    bajty_po = len(zakodowany.tobytes())
    skutecznosc = (1 - bajty_po / bajty_przed) * 100
    wielkosc_slownika = (len(d.codebook) * (ilosc_bajtow * 8 + len(next(d.codebook.values().__iter__())))) / 8
    return skutecznosc, bajty_po, wielkosc_slownika

def home(request):
    payload = {
        'odkodowany': '',
        'zakodowany': ''
    }
    tekst = request.GET.get('tekst', '')
    akcja = request.GET.get('akcja', '')
    if akcja == 'zakoduj' and len(tekst) > 0:
        wagi = countchars(tekst)
        d = Drzewo(wagi)
        payload['akcja'] = akcja
        payload['zakodowany'] = encode(tekst, d.codebook).unpack(zero=b'0', one=b'1').decode('utf-8')
        payload['zakodowany_bajtowo'] = encode(tekst, d.codebook).tobytes()
        payload['odkodowany'] = tekst
        payload['wagi'] = wagi
        payload['znaki'] = []
        payload['wystapienia'] = []
        payload['reprezentacja'] = {}
        payload['bajty_przed'] = len(tekst)
        payload['bajty_po'] = len(payload['zakodowany_bajtowo'])
        payload['skutecznosc'] = (1 - payload['bajty_po'] / payload['bajty_przed']) * 100
        for key, value in wagi.items():
            payload['znaki'].append(key)
            payload['wystapienia'].append(value)
        for key, value in d.codebook.items():
            payload['reprezentacja'][key] = value.unpack(zero=b'0', one=b'1').decode('utf-8')
    if request.POST.get('akcja', '') == 'zakoduj_plik':
        file = request.FILES['file'].read()
        payload['bajty_przed'] = len(file)
        payload['akcja'] = 'zakoduj_plik'

        payload['pomiary'] = []

        wagi = countchars(file)
        d = Drzewo(wagi)
        zakodowany = encode(file, d.codebook)
        bajty_po = len(zakodowany.tobytes())
        skutecznosc = (1 - bajty_po / payload['bajty_przed']) * 100
        slownik = (len(d.codebook) * (8 + len(next(d.codebook.values().__iter__())))) / 8
        payload['pomiary'].append({
            'klucz': 1,
            'bajty_po': bajty_po,
            'skutecznosc': skutecznosc,
            'slownik': slownik,
            'bajty_po_ze_slownikiem': int(bajty_po + slownik),
            'skutecznosc_prawdziwa': (1 - (bajty_po + slownik) / payload['bajty_przed']) * 100
        })

        for i in [2, 3, 10, 20, 50, 100, 512, 1024]:
            skutecznosc, bajty_po, slownik = zakoduj_plik(file, i)
            payload['pomiary'].append({
                'klucz': i,
                'bajty_po': bajty_po,
                'skutecznosc': skutecznosc,
                'slownik': slownik,
                'bajty_po_ze_slownikiem': int(bajty_po + slownik),
                'skutecznosc_prawdziwa': (1 - (bajty_po + slownik) / payload['bajty_przed']) * 100
            })

    return render(request, 'index.html', payload)
