from huffman.binarytree import Drzewo
from huffman.countchars import countchars
from huffman.huffman import encode

tekst = 'Siała baba mak, nie weidziała jak, Jakiś sobie taki nijaki tekst testowy'
wagi = countchars(tekst)
d = Drzewo(wagi)
zakodowany = encode(tekst, d.codebook)
print('Kodowany tekst: {}'.format(tekst))
print('Słownik: {}'.format(d.codebook))
print('Zakodowana postać binarna: {}'.format(zakodowany))
print('Zakodowana postać bajtowa: {}'.format(zakodowany.tobytes()))
print('Odkodowana postać bajtowa: {}'.format(''.join(zakodowany.decode(d.codebook))))
